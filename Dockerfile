FROM adoptopenjdk/openjdk11:slim
ENV AM_VERSION "5.7.4.7416-UAT"
RUN mkdir -p /opt/am/${AM_VERSION}
ADD AccessMatrix-${AM_VERSION}-tomcat-only.tar.gz /opt/am/${AM_VERSION}
RUN groupadd am \
    && useradd -r -g am -s /bin/bash am \
    && chown -R am:am /opt/am/${AM_VERSION} \
    && chmod 777 /opt/am/${AM_VERSION}/tomcat/bin/*.sh \
	&& chmod 777 /opt/am/${AM_VERSION}/tomcat/logs/ \
    && ln -s /opt/am/${AM_VERSION} /opt/am/${AM_VERSION} \
    && chown -R am:am /opt/am/${AM_VERSION}
COPY ["amsystem.properties", "/opt/am/${AM_VERSION}/tomcat/webapps/am5/WEB-INF/classes/"]  
EXPOSE 8080 8443
USER root

CMD ["/opt/am/5.7.4.7416-UAT/tomcat/bin/catalina.sh", "run"]